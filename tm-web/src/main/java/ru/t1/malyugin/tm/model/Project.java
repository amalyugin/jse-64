package ru.t1.malyugin.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.malyugin.tm.enumerated.Status;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public final class Project implements Serializable {

    private static final long serialVersionUID = 1;

    private String id = UUID.randomUUID().toString();

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date start = new Date();

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date finish;

    private String name = "";

    private String description = "";

    private Status status = Status.NOT_STARTED;

    public Project(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

}